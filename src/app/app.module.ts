import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FrequentProceduresComponent } from './components/frequent-procedures/frequent-procedures.component';
import { InformationComponent } from './components/information/information.component';
import { OtherTopicsComponent } from './components/other-topics/other-topics.component';
import { GiveUsYourOpinionComponent } from './components/give-us-your-opinion/give-us-your-opinion.component';
import { HttpClientModule } from '@angular/common/http';
import { BannerComponent } from './components/banner/banner.component';
import { MainBoardComponent } from './components/main-board/main-board.component';
import { ParticipantComponent } from './components/participant/participant.component';
import { CardinfoComponent } from './components/cardinfo/cardinfo.component';
import { CardInfoDoubleComponent } from './components/card-info-double/card-info-double.component';
import { MomentModule } from 'ngx-moment';
import { TopicCardComponent } from './components/topic-card/topic-card.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HomeComponent,
    FrequentProceduresComponent,
    InformationComponent,
    OtherTopicsComponent,
    GiveUsYourOpinionComponent,
    BannerComponent,
    MainBoardComponent,
    ParticipantComponent,
    CardinfoComponent,
    CardInfoDoubleComponent,
    TopicCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MomentModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
