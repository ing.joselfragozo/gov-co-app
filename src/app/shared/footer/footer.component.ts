import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { stringify } from 'querystring';
import { Observable } from 'rxjs';

@Component({
  selector: 'gov-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  GeneralInfo: Observable<any>
  Dataloaded = false;
  DataFooter: any 

  constructor(private firebaseDatabase : AngularFireDatabase) { }

  ngOnInit(): void {
    this.firebaseDatabase.object("base").valueChanges().subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.DataFooter = data;
  }

  onErrorDataLoaded(error) {
    console.log(error)
  }
}
