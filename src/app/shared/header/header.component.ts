import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'gov-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  GeneralInfo: Observable<any>
  Dataloaded: boolean = false;
  Dataheader: any

  constructor(private firebaseDatabase : AngularFireDatabase) { }

  btnSearchGovCo() { }

  changeLang() { }

  ngOnInit(): void {
    this.firebaseDatabase.object("base").valueChanges().subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Dataheader = data;
  }

  onErrorDataLoaded(error) {
    console.log(error);
  }
}
