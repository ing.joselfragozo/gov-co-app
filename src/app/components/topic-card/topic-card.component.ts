import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-topic-card',
  templateUrl: './topic-card.component.html',
  styleUrls: ['./topic-card.component.css']
})
export class TopicCardComponent implements OnInit {

  @Input()
  title: string = "title";

  @Input()
  image: string = "link";

  @Input()
  link: string = "link";

  @Input()
  description: string = "description"

  constructor() { }

  ngOnInit(): void {
  }

}
