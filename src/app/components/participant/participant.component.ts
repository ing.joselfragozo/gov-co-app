import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {

  @Input()
  color: string;

  @Input()
  title: string;

  @Input()
  topic: string;

  @Input()
  institucion: string;

  @Input()
  contador: number;

  constructor() { }

  ngOnInit(): void {
  }

}
