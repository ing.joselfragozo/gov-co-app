import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardinfo',
  templateUrl: './cardinfo.component.html',
  styleUrls: ['./cardinfo.component.css']
})
export class CardinfoComponent implements OnInit {

  @Input()
  background: string = "url('../../../assets/images/information/2.png')"

  @Input()
  date: any = Date();

  @Input()
  message: string = "message";

  formats: string[] = ['DD/MM/YYYY HH:mm:ss', 'DD/MM/YYYY HH:mm'];

  constructor() { }

  ngOnInit(): void {
  }

}
