import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-info-double',
  templateUrl: './card-info-double.component.html',
  styleUrls: ['./card-info-double.component.css']
})
export class CardInfoDoubleComponent implements OnInit {

  @Input()
  date: any = Date();

  @Input()
  message: string = "message";

  formats: string[] = ['DD/MM/YYYY HH:mm:ss', 'DD/MM/YYYY HH:mm'];

  constructor() { }

  ngOnInit(): void {
  }

}
