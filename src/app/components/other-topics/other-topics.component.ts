import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-other-topics',
  templateUrl: './other-topics.component.html',
  styleUrls: ['./other-topics.component.css']
})
export class OtherTopicsComponent implements OnInit {

  Dataloaded: boolean;
  Topics: any;

  constructor(private firebaseDatabase: AngularFireDatabase) { }

  ngOnInit(): void {
    this.firebaseDatabase.object("topic").valueChanges().subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Topics = data.topics;
  }

  onErrorDataLoaded(error) {
    console.log(error);
  }
}
