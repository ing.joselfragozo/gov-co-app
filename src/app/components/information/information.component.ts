import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  
  Cards: any;
  Dataloaded: boolean = false;

  constructor(private firebaseDatabase : AngularFireDatabase) { }

  ngOnInit(): void {
    this.Cards = this.firebaseDatabase.object("cards").valueChanges();
    this.Cards.subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Cards = data;
  }

  onErrorDataLoaded(error) {
    console.log(error)
  }
}
