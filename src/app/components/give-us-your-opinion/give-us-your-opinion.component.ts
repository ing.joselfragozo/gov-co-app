import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-give-us-your-opinion',
  templateUrl: './give-us-your-opinion.component.html',
  styleUrls: ['./give-us-your-opinion.component.css']
})
export class GiveUsYourOpinionComponent implements OnInit {

  Dataloaded = false;
  Tags: any = []
  opinions: any = []
  yourVariable: string = "jose";
  contador: number = 1;
  textareaValue: string = "";
  Opinions: Observable<any>;
  isFirstLoad: Boolean = true;

  constructor(private firebaseDatabase: AngularFireDatabase) { }

  ngOnInit(): void {
    this.isFirstLoad = true;
    this.firebaseDatabase.object("opinion").valueChanges().subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });

    this.firebaseDatabase.object("user_opinion").valueChanges().subscribe({
      next: this.onDataOpinionLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Tags = data.tags;
  }

  onErrorDataLoaded(error) {
    console.log(error)
  }

  onDataOpinionLoaded(data) {
    this.opinions = data.opinions || [];
    let count = this.opinions.length;

    if (count > this.contador && !this.isFirstLoad) {
      alert(" hay registradas " + this.contador + " opiniones, su opinion ha sido guardada exitosamente");
    }

    if (this.isFirstLoad) {
      this.isFirstLoad = false;
    }
    this.contador = count;
  }

  sendOpinion() {
    if (this.textareaValue.length > 0) {
      this.firebaseDatabase.object("user_opinion").set({ opinions: [...this.opinions, this.textareaValue] });
      this.textareaValue = ""
    } else {
      alert("debe escribir una opinion");
    }
  }
}
